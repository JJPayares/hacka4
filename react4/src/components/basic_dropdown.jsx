export function BasicDropdown({ modelo, list_url, create_element_url }) {
    return (
        <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" href={list_url} id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                {modelo}
            </a>
            <ul className="dropdown-menu" aria-labelledby="navbarDropdown">
                {create_element_url ?
                    <>
                        <li><a className="dropdown-item" href={create_element_url}>Nuevo</a></li>
                        <li><hr className="dropdown-divider" /></li>
                    </> : ""
                }
                {list_url ?
                    <>
                        <li><a className="dropdown-item" href={list_url}>Listar</a></li>
                        <li><hr className="dropdown-divider" /></li>
                    </> : ""
                }
            </ul>
        </li>
    )
}