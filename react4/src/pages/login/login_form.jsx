import { Field, Form, Formik } from 'formik';
import React from 'react';
import { getToken } from '../../services/get_token';
import { useNavigate } from 'react-router-dom';


export function LoginForm({ innerRef }) {
    const nav = useNavigate();
    const datos_iniciales = {
        username: '',
        password: '',
    };

    const submit = (userData, actions) => {
        actions.setSubmitting(true);
        getToken(userData);
        actions.resetForm();
        actions.setSubmitting(false);
        nav('/')
    };


    return (
        <Formik
            initialValues={datos_iniciales}
            onSubmit={submit}
            innerRef={innerRef}
        >
            {({ }) => {
                return (
                    <Form>
                        <div
                            className="form-group p-3"
                            style={{
                                outline: '1px solid blue',
                                borderRadius: '10px',
                            }}
                        >
                            <label htmlFor="username">Nombre de usuario</label>
                            <Field
                                className="form-control"
                                id="username"
                                type="text"
                                name="username"
                                placeholder="Ingrese su nombre de usuario"
                            />

                            <label htmlFor="pass">Contraseña</label>
                            <Field
                                className="form-control"
                                id="pass"
                                type="password"
                                name="password"
                                placeholder="Ingrese su contraseña"
                            />
                            <button type="submit">Login</button>
                        </div>
                    </Form>
                );
            }}
        </Formik>
    );
}
