import { useState, useEffect } from 'react';
import { ActionsButtons } from '../../components/actions_buttons';
import { DeleteModal } from '../../components/delete_modal';
import axiosInstance from '../../services/axios_instance';
import { Delete } from '../../services/delete';
export function ScheduleElement({ details }) {

    let [nombre, setNombre] = useState("")
    const url = `doctors/${details?.doctor_id}/`

    useEffect(() => {
        axiosInstance({
            url: url, method: 'get'
        }).then((response) => {
            let res = response.data
            setNombre(res.name + ' ' + res.last_name)
        })
            .catch((e) => {
                return e.response
            })
    }, [details])

    function onClick() {
        Delete(url)
    }

    return (
        <div>
            <div className="card" >
                <div className="py-3">
                    <img src="https://picsum.photos/100/100" className="card-img-top resize" style={{ maxWidth: '18rem', position: 'relative' }} />
                </div>
                <div className="card-body">
                    <h5 className="card-title">Dr. {nombre}</h5>
                    <ul className="list-group">
                        <li className="list-group-item">Fecha de tratamiento: {details?.treated_at}</li>
                        <li className="list-group-item">Inicio: {details?.treatment_start}</li>
                        <li className="list-group-item">Fin: {details?.treatment_end}</li>
                    </ul>
                </div>
                <ActionsButtons editUrl={`/schedules/edit/${details?.id}`} id={details?.id} />
                <DeleteModal id={details?.id} deleteFunction={onClick} />
            </div>
        </div>
    )
}