import { useNavigate } from 'react-router-dom';
export function DeleteModal({ id, deleteFunction }) {


    let nav = useNavigate()

    return (
        <div className="modal fade" id={`id${id}`} tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        Esta seguro que quiere borrar el registro?
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" className="btn btn-danger" onClick={() => {
                            deleteFunction();
                            nav(-1)
                        }} >Confirm delete</button>
                    </div>
                </div>
            </div>
        </div>
    )
}