from django.contrib import admin
from api.models import *


# Register your models here.
@admin.register(Doctor)
class AdminDoctor(admin.ModelAdmin):
    pass


@admin.register(Specialty)
class AdminSpecialty(admin.ModelAdmin):
    pass


@admin.register(Doctor_Specialty)
class AdminDoctor_Specialty(admin.ModelAdmin):
    pass


@admin.register(Schedule)
class AdminSchedule(admin.ModelAdmin):
    pass

@admin.register(Patient)
class AdminPatient(admin.ModelAdmin):
    pass

@admin.register(Appointment)
class AdminAppointment(admin.ModelAdmin):
    pass


