
import { DetailButton } from '../../components/datail_button';
import { useEffect, useState } from 'react';
import axiosInstance from '../../services/axios_instance';


export function ScheduleRow({ schedule }) {
    let [nombre, setNombre] = useState("")
    const url = `doctors/${schedule?.doctor_id}/`

    useEffect(() => {
        axiosInstance({
            url: url, method: 'get'
        }).then((response) => {
            let res = response.data
            setNombre(res.name + ' ' + res.last_name)
        })
            .catch((e) => {
                return e.response
            })
    }, [])

    return (
        <tr>
            <th scope="row">{schedule.id}</th>
            <td>{schedule.treated_at}</td>
            <td>{nombre}</td>
            <td> <DetailButton id={schedule.id} /> </td>
        </tr>
    )
}