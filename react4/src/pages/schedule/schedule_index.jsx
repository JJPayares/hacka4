import { useState, useEffect } from 'react';
import axiosInstance from '../../services/axios_instance';
import { ListSchedule } from './list_schedules';


export function Schedule() {
    let [schedules, setSchedules] = useState(null)
    const SCHEDULE_URL = "schedules/"

    useEffect(() => {
        axiosInstance({ url: SCHEDULE_URL })
            .then((response) => {
                let results = response.data
                setSchedules(results)
            })
    }, [])

    return (
        <>
            <h1>HORARIOS</h1>

            <div>
                {schedules
                    ? <ListSchedule schedules={schedules} />
                    : 'Cargando...'}
            </div>
        </>
    )
}