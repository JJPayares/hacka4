import axiosInstance from './axios_instance';

export function getToken(credentials) {

    axiosInstance({
        url: 'token/', method: 'post', data: {
            "username": credentials.username,
            "password": credentials.password,
        }
    }).then((response) => {
        localStorage.setItem('refresh_token', JSON.stringify(response.data.refresh))
        localStorage.setItem('access_token', JSON.stringify(response.data.access))
        localStorage.setItem('user', credentials.username)
    }).catch((e) => {
        let e_response = e.response
        localStorage.clear()
        return e_response
    })

}
