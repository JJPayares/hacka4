# Generated by Django 3.2.13 on 2022-06-04 16:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20220604_1650'),
    ]

    operations = [
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('dni', models.CharField(max_length=50)),
                ('address', models.CharField(max_length=150)),
                ('phone', models.CharField(max_length=50)),
                ('gender', models.CharField(max_length=50)),
                ('born_date', models.DateField()),
                ('registered_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('registered_by', models.CharField(blank=True, max_length=50)),
                ('modified_by', models.CharField(blank=True, max_length=50)),
                ('active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('treated_at', models.DateField()),
                ('treatment_start', models.DateTimeField(auto_now_add=True)),
                ('treatment_end', models.DateTimeField(auto_now_add=True)),
                ('status', models.BooleanField(default=True)),
                ('description', models.CharField(max_length=150)),
                ('active', models.BooleanField(default=True)),
                ('registered_at', models.DateTimeField(auto_now_add=True)),
                ('registered_by', models.CharField(blank=True, max_length=50)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('modified_by', models.CharField(blank=True, max_length=50)),
                ('doctor_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.doctor')),
                ('patient_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.patient')),
            ],
        ),
    ]
