import { useEffect, useState } from 'react';
import { useParams } from "react-router-dom"
import { DoctorCV } from './doctor_cv';
import axiosInstance from '../../services/axios_instance';


export function DoctorDetails() {
    const [details, setDetails] = useState(null)
    const [error, setError] = useState(null)

    const params = useParams()
    const url = `doctors/${params.id}/`

    useEffect(() => {
        axiosInstance({
            url: url, method: 'get'
        }).then((response) => setDetails(response.data))
            .catch((e) => {
                return e.response
            })
    }, [])
    
    return (
        <div>
            <DoctorCV details={details} key={details?.id }/>
        </div>

    )
}