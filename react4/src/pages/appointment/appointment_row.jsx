import { DetailButton } from "../../components/datail_button";
import { useState, useEffect } from 'react';
import axiosInstance from '../../services/axios_instance';


export function AppointmentRow({ appointment }) {

    let [nombre_pac, setNombrePac] = useState("")
    const url_patients = `patients/${appointment?.patient_id}/`
    useEffect(() => {

        axiosInstance({
            url: url_patients, method: 'get'
        }).then((response) => {
            let res = response.data
            setNombrePac(res.name + ' ' + res.last_name)
        })
            .catch((e) => {
                return e.response
            })


    }, [appointment])
    return (
        <tr>
            <th scope="row">{appointment.id}</th>
            <td>{nombre_pac}</td>
            <td>{appointment.treated_at}</td>
            <td> <DetailButton id={appointment.id} key={appointment.id} /> </td>
        </tr>
    )
}