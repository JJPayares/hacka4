export function ActionsButtons({ editUrl, id }) {
    return (
        <div className="d-grid gap-2 d-md-block">
            <a className="btn btn-primary mx-1" href={editUrl} role="button">Edit</a>
            <button type="button" className="btn btn-danger mx-1" data-bs-toggle="modal" data-bs-target={`#id${id}`}>
                Delete
            </button>
        </div>
    )
}