import axios from 'axios'

let TOKEN = JSON.parse(localStorage.getItem('access_token'))


const axiosInstance = axios.create({
    baseURL: 'http://127.0.0.1:8000/api/',
    headers: {
        'Authorization': `Bearer ${TOKEN}`,
        'Content-Type': 'application/json',
        'accept': 'application/json',
    }
});

export default axiosInstance;