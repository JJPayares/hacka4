import { useState, useEffect } from 'react';
import axiosInstance from '../../services/axios_instance';
import { ListDoctors } from './list_doctors';


export function Doctor() {
    let [doctors, setDoctors] = useState(null)
    const DOCTORS_URL = "doctors/"


    useEffect(() => {
        axiosInstance({ url: DOCTORS_URL })
            .then((response) => {
                let results = response.data
                setDoctors(results)

            })
    }, [])

    return (
        <>
            <h1>DOCTORES</h1>

            <div>
                {doctors
                    ? <ListDoctors doctors={doctors} />
                    : 'Cargando...'}
            </div>
        </>
    )
}