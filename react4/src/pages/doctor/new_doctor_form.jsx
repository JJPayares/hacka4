import { Field, Form, Formik } from 'formik';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { createElement } from '../../services/create_instance';


export function NewDoctorForm({ innerRef }) {
    const nav = useNavigate()
    const new_doctor_url = 'doctors/'

    let loggedUser = localStorage.getItem('user')
    const datos_iniciales = {
        name: "",
        last_name: "",
        dni: "",
        address: "",
        email: "",
        phone: "",
        gender: "Masculino",
        tuition_num: "",
        born_date: "",
        registered_by: loggedUser,
        modified_by: loggedUser,
    }

    const submit = (doctorValues, actions) => {
        actions.setSubmitting(true);
        //console.log(doctorValues)
        actions.resetForm();
        //createDoctor(doctorValues);
        createElement(doctorValues, new_doctor_url)
        actions.setSubmitting(false);
        nav('/doctors/')
    };

    return (
        <Formik
            initialValues={datos_iniciales}
            onSubmit={submit}
            innerRef={innerRef}
        >
            {({ }) => {
                return (
                    <Form>
                        <div
                            className="form-group p-3"
                            style={{
                                outline: '1px solid blue',
                                borderRadius: '10px',
                            }}
                        >
                            <label htmlFor="name">Nombre</label>
                            <Field
                                className="form-control"
                                id="name"
                                type="text"
                                name="name"
                                placeholder="Ingrese el nombre del medico."
                                required
                            />

                            <label htmlFor="last_name">Apellido</label>
                            <Field
                                className="form-control"
                                id="last_name"
                                type="text"
                                name="last_name"
                                placeholder="Ingrese el apellido del medico."
                                required
                            />

                            <label htmlFor="dni">Documento de identidad</label>
                            <Field
                                className="form-control"
                                id="dni"
                                type="text"
                                name="dni"
                                placeholder="Ingrese el DNI del medico."
                                required
                            />

                            <label htmlFor="address">Direccion</label>
                            <Field
                                className="form-control"
                                id="address"
                                type="text"
                                name="address"
                                placeholder="Ingrese la direcion del medico."
                                required
                            />

                            <label htmlFor="email">Correo electronico</label>
                            <Field
                                className="form-control"
                                id="email"
                                type="email"
                                name="email"
                                placeholder="Ingrese el correo electronico del medico."
                                required
                            />

                            <label htmlFor="phone">Telefono</label>
                            <Field
                                className="form-control"
                                id="phone"
                                type="text"
                                name="phone"
                                placeholder="Ingrese el numero de elefono del medico."
                                required
                            />

                            <label htmlFor="gender">Genero</label>
                            <Field name="gender" as="select" def>
                                <option value="Femenino">Femenino</option>
                                <option value="Masculino">Masculino</option>
                                <option value="Otros">Otros</option>
                            </Field>

                            <label htmlFor="tuition_num">Num Colegiatura</label>
                            <Field
                                className="form-control"
                                id="tuition_num"
                                type="text"
                                name="tuition_num"
                                placeholder="Ingrese el num. de colegiatura del medico."
                                required
                            />

                            <label htmlFor="born_date">Fecha de nacimiento</label>
                            <Field
                                className="form-control"
                                id="born_date"
                                type="date"
                                name="born_date"
                                placeholder="Ingrese la fecha de naciemiento del medico."
                                required
                            />

                            <button type="submit">Agregar</button>
                        </div>
                    </Form>
                );
            }}
        </Formik>
    );
}
