import { Field, Form, Formik } from 'formik';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import { createElement } from '../../services/create_instance';


export function NewPatientForm({ innerRef }) {
    const nav = useNavigate()
    const new_patient_url = 'patients/'

    let loggedUser = localStorage.getItem('user')
    const datos_iniciales = {
        name: "",
        last_name: "",
        dni: "",
        address: "",
        phone: "",
        gender: "Masculino",
        born_date: "",
        registered_by: loggedUser,
        modified_by: "",
    }

    const submit = (patientValues, actions) => {
        actions.setSubmitting(true);
        console.log(patientValues)
        //actions.resetForm();
        createElement(patientValues, new_patient_url)
        actions.setSubmitting(false);
        nav('/patients/')
    };

    return (
        <Formik
            initialValues={datos_iniciales}
            onSubmit={submit}
            innerRef={innerRef}
        >
            {({ }) => {
                return (
                    <Form>
                        <div
                            className="form-group p-3"
                            style={{
                                outline: '1px solid blue',
                                borderRadius: '10px',
                            }}
                        >
                            <label htmlFor="name">Nombre</label>
                            <Field
                                className="form-control"
                                id="name"
                                type="text"
                                name="name"
                                placeholder="Ingrese el nombre del paciente."
                                required
                            />

                            <label htmlFor="last_name">Apellido</label>
                            <Field
                                className="form-control"
                                id="last_name"
                                type="text"
                                name="last_name"
                                placeholder="Ingrese el apellido del paciente."
                                required
                            />

                            <label htmlFor="dni">Documento de identidad</label>
                            <Field
                                className="form-control"
                                id="dni"
                                type="text"
                                name="dni"
                                placeholder="Ingrese el DNI del paciente."
                                required
                            />

                            <label htmlFor="address">Direccion</label>
                            <Field
                                className="form-control"
                                id="address"
                                type="text"
                                name="address"
                                placeholder="Ingrese la direcion del paciente."
                                required
                            />

                            <label htmlFor="phone">Telefono</label>
                            <Field
                                className="form-control"
                                id="phone"
                                type="text"
                                name="phone"
                                placeholder="Ingrese el numero de telefono del paciente."
                                required
                            />

                            <label htmlFor="gender">Genero</label>
                            <Field name="gender" as="select" def>
                                <option value="Femenino">Femenino</option>
                                <option value="Masculino">Masculino</option>
                                <option value="Otros">Otros</option>
                            </Field>

                            <label htmlFor="born_date">Fecha de nacimiento</label>
                            <Field
                                className="form-control"
                                id="born_date"
                                type="date"
                                name="born_date"
                                placeholder="Ingrese la fecha de naciemiento del paciente."
                                required
                            />

                            <button type="submit">Agregar</button>
                        </div>
                    </Form>
                );
            }}
        </Formik>
    );
}
