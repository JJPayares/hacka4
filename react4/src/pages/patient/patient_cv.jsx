import { ActionsButtons } from "../../components/actions_buttons";
import { DeleteModal } from "../../components/delete_modal";
import { Delete } from "../../services/delete";



export function PatientCV({ details }) {

    const url = `patients/${details?.id}`

    function onClick() {
        Delete(url)
    }

    return (
        <div>
            <div className="card" >
                <div className="py-3">
                    <img src="https://picsum.photos/100/100" className="card-img-top resize" style={{ maxWidth: '18rem', position: 'relative' }} />
                </div>
                <div className="card-body">
                    <h5 className="card-title">{details?.name} {details?.last_name}</h5>
                    <ul className="list-group">
                        <li className="list-group-item">DNI # {details?.dni}</li>
                        <li className="list-group-item">Genero {details?.gender}</li>
                        <li className="list-group-item">Telefono {details?.phone}</li>
                        <li className="list-group-item">Direccion {details?.address}</li>
                        <li className="list-group-item">Fecha de nacimiento {details?.born_date}</li>
                    </ul>
                    <br />
                    <ActionsButtons editUrl={`/patients/edit/${details?.id}`} id={details?.id} />
                    <DeleteModal id={details?.id} deleteFunction={onClick} />
                </div>
            </div>
        </div>
    )
}