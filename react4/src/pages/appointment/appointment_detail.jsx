import { useEffect, useState } from 'react';
import { useParams } from "react-router-dom"
import axiosInstance from '../../services/axios_instance';
import { AppointmentElement } from './appointment_element';


export function AppointmentDetails() {
    const [details, setDetails] = useState(null)

    const params = useParams()
    const url = `appointments/${params.id}/`

    useEffect(() => {
        axiosInstance({
            url: url, method: 'get'
        }).then((response) => setDetails(response.data))
            .catch((e) => {
                return e.response
            })
    }, [])

    return (
        <div>
            <AppointmentElement details={details} key={details?.id} />
        </div>

    )
}