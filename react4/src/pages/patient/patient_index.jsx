import { useState, useEffect } from 'react';
import axiosInstance from '../../services/axios_instance';
import { ListPatients } from './list_patients';



export function Patients() {
    let [patients, setPatients] = useState(null)
    const PATIENTS_URL = "patients/"


    useEffect(() => {
        axiosInstance({ url: PATIENTS_URL })
            .then((response) => {
                let results = response.data
                setPatients(results)

            })
    }, [])

    return (
        <>
            <h1>PACIENTES</h1>

            <div>
                {patients
                    ? <ListPatients patients={patients} />
                    : 'Cargando...'}
            </div>
        </>
    )
}