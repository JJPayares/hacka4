import axiosInstance from './axios_instance';

export function updateElement(values, url) {
    axiosInstance({
        url: url, method: 'put', data: values
    }).then((response) => {
        if (response.statusText === "OK") {
            return response
        }
    }).catch((e) => {
        let e_response = e.response
        return e_response
    })

}
