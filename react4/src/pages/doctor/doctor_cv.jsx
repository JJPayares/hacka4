import { ActionsButtons } from "../../components/actions_buttons";
import { DeleteModal } from "../../components/delete_modal";
import { Delete } from "../../services/delete";



export function DoctorCV({ details }) {

    const url = `doctors/${details?.id}`

    function onClick() {
        Delete(url)
    }

    return (
        <div>
            <div className="card" >
                <div className="py-3">
                    <img src="https://picsum.photos/100/100" className="card-img-top resize" style={{ maxWidth: '18rem', position: 'relative' }} />
                </div>
                <div className="card-body">
                    <h5 className="card-title">Dr. {details?.name} {details?.last_name}</h5>
                    <ul className="list-group">
                        <li className="list-group-item">Matricula profesional # {details?.tuition_num}</li>
                        <li className="list-group-item">DNI # {details?.dni}</li>
                        <li className="list-group-item">Genero {details?.gender}</li>
                        <li className="list-group-item">Telefono {details?.phone}</li>
                        <li className="list-group-item">Email {details?.email}</li>
                        <li className="list-group-item">Direccion {details?.address}</li>
                        <li className="list-group-item">Fecha de nacimiento {details?.born_date}</li>
                    </ul>
                    <br/>
                    <ActionsButtons editUrl={`/doctors/edit/${details?.id}`} id={details?.id} />
                    <DeleteModal id={details?.id} deleteFunction={onClick}/>
                </div>
            </div>
        </div>
    )
}