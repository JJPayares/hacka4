
import { AppointmentRow } from './appointment_row';
export function ListAppointment({ appointments }) {

    return (
        <div>

            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Paciente</th>
                        <th scope="col">Fecha de tratamiento</th>
                        <th scope="col">Detail</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments
                        ? appointments.results?.map((appointment) => {
                            return (
                                <AppointmentRow appointment={appointment} />
                            )

                        })
                        : 'Cargando...'}
                </tbody>
            </table>

        </div>
    )
}