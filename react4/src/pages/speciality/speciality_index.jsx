import { useState, useEffect } from 'react';

import axiosInstance from '../../services/axios_instance';

import { ListSpeciality } from './list_specialitys';

export function Speciality() {
    let [specialitys, setSpeciality] = useState(null);
    const SPECIALITY_URL = "specialitys/"


    useEffect(() => {
        axiosInstance({ url: SPECIALITY_URL })
            .then((response) => {
                let results = response.data
                setSpeciality(results)
            })
    }, [])

    return (
        <>
            <h1>ESPECIALIDADES</h1>

            <div>
                {specialitys
                    ? <ListSpeciality specialitys={specialitys} />
                    : 'Cargando...'}
            </div>
        </>
    )
}