import { Field, Form, Formik } from 'formik';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import axiosInstance from '../../services/axios_instance';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { updateElement } from '../../services/update_instance';



export function EditAppointment({ innerRef }) {
    const nav = useNavigate()
    const param = useParams()

    //All doctors & patients
    let [doctors, setDoctors] = useState(null)
    let [patients, setPatients] = useState(null)

    let [current, setCurrent] = useState(null)

    const new_appointment_url = 'appointments/'
    const current_appointment = `appointments/${param?.id}/`

    useEffect(() => {
        //current values
        axiosInstance(current_appointment)
            .then((res) => setCurrent(res.data))
            .catch((e) => e)

        //All doctors
        axiosInstance('doctors/')
            .then((res) => setDoctors(res.data))
            .catch((e) => e)

        //All patients
        axiosInstance('patients/')
            .then((res) => setPatients(res.data))
            .catch((e) => e)
    }, [])

    let loggedUser = localStorage.getItem('user')

    const submit = (appointmentsValues, actions) => {
        actions.setSubmitting(true);
        console.log(appointmentsValues)
        appointmentsValues.modified_by = loggedUser
        delete appointmentsValues.id, delete appointmentsValues.modified_at, delete appointmentsValues.resgitered_at, delete appointmentsValues.treatment_start, delete appointmentsValues.treatment_end
        //actions.resetForm();
        updateElement(appointmentsValues, current_appointment)
        actions.setSubmitting(false);
        //nav('/appointments/')
    };

    return (
        <>

            {current ? <Formik
                initialValues={current}
                onSubmit={submit}
                innerRef={innerRef}
            >
                {({ }) => {
                    return (
                        <Form>
                            <div
                                className="form-group p-3"
                                style={{
                                    outline: '1px solid blue',
                                    borderRadius: '10px',
                                }}
                            >
                                <label htmlFor="treated_at">Fecha de tratamiento</label>
                                <Field
                                    className="form-control"
                                    id="treated_at"
                                    type="date"
                                    name="treated_at"
                                    placeholder="Ingrese la fecha de tratamiento."
                                    required
                                />
                                <br />

                                <label htmlFor="doctor_id" required>Doctor</label>
                                <Field name="doctor_id" as="select" def>
                                    {doctors ? doctors?.results.map((doctor) => {
                                        return (
                                            <option key={doctor.id} value={doctor.id}>{`${doctor.name} ${doctor.last_name}`}</option>)
                                    }) : "Cargando..."}
                                </Field>
                                <br />

                                <label htmlFor="patient_id" required>Paciente</label>
                                <Field name="patient_id" as="select" def>
                                    {patients ? patients?.results.map((patient) => {
                                        return (
                                            <option key={patient.id} value={patient.id}>{`${patient.name} ${patient.last_name}`}</option>)
                                    }) : "Cargando..."}
                                </Field>
                                <br />

                                <label htmlFor="description">Descripcion</label>
                                <Field
                                    className="form-control"
                                    id="description"
                                    type="text"
                                    name="description"
                                    placeholder="Ingrese la descripcion de la cita."
                                    required
                                />
                                <br />

                                <label htmlFor="status">Atencion</label>
                                <Field name="status" as="select">
                                    <option value={true}>Completada</option>
                                    <option value={false}>Abierta</option>
                                </Field>

                                <label htmlFor="active">Estado</label>
                                <Field name="active" as="select">
                                    <option value={true}>Activo</option>
                                    <option value={false}>Inactivo</option>
                                </Field>


                                <button type="submit">Agregar</button>
                            </div>
                        </Form>
                    );
                }}
            </Formik> : 'Cargando....'}
        </>
    );
}
