export function DetailButton({ id }) {
    return (
        <a className="btn btn-success" href={`${id}/`} role="button">Detalles</a>
    )
}