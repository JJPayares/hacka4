import { useState, useEffect } from 'react';
import { ActionsButtons } from '../../components/actions_buttons';
import { DeleteModal } from '../../components/delete_modal';
import axiosInstance from '../../services/axios_instance';
import { Delete } from '../../services/delete';
export function AppointmentElement({ details }) {
    const url = `appointments/${details?.id}/`

    let [nombre_doc, setNombreDoc] = useState("")
    const url_doctors = `doctors/${details?.doctor_id}/`

    let [nombre_pac, setNombrePac] = useState("")
    const url_patients = `patients/${details?.patient_id}/`

    useEffect(() => {

        axiosInstance({
            url: url_doctors, method: 'get'
        }).then((response) => {
            let res = response.data
            setNombreDoc(res.name + ' ' + res.last_name)
        })
            .catch((e) => {
                return e.response
            })


        axiosInstance({
            url: url_patients, method: 'get'
        }).then((response) => {
            let res = response.data
            setNombrePac(res.name + ' ' + res.last_name)
        })
            .catch((e) => {
                return e.response
            })


    }, [details])

    function onClick() {
        Delete(url)
    }

    console.log('doc', details)
    return (
        <div>
            <div className="card" >
                <div className="py-3">
                    <img src="https://picsum.photos/100/100" className="card-img-top resize" style={{ maxWidth: '18rem', position: 'relative' }} />
                </div>
                <div className="card-body">
                    <h5 className="card-title">Dr. {nombre_doc}</h5>
                    <h5 className="card-title">Paciente {nombre_pac}</h5>
                    <ul className="list-group">
                        <li className="list-group-item">Fecha de tratamiento: {details?.treated_at} </li>
                        <li className="list-group-item">Inicio: {details?.treatment_start}</li>
                        <li className="list-group-item">Fin: {details?.treatment_end}</li>
                        <li className="list-group-item">Detalles: <p>{details?.description}</p></li>
                    </ul>
                </div>
                <ActionsButtons editUrl={`/appointments/edit/${details?.id}`} id={details?.id} />
                <DeleteModal id={details?.id} deleteFunction={onClick} />
            </div>
        </div>
    )
}