import { DetailButton } from "../../components/datail_button"



export function SpecialityRow({ speciality }) {
    return (
        <tr>
            <th scope="row">{speciality.id}</th>
            <td>{speciality.name}</td>
            <td>{speciality.description}</td>
            <td> <DetailButton id={speciality.id} /> </td>
        </tr>
    )
}