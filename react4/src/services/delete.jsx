import axiosInstance from './axios_instance';

export function Delete(url) {
    axiosInstance({
        url: url, method: 'delete'
    }).then((response) => {
        if (response.statusText === "OK") {
            console.log(response)
            return response
        }
    }).catch((e) => {
        let e_response = e.response
        return e_response
    })

}
