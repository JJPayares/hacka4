
import { SpecialityRow } from './speciality_row';
export function ListSpeciality({ specialitys }) {
    return (
        <div>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Description</th>
                        <th scope="col">Detail</th>
                    </tr>
                </thead>
                <tbody>
                    {specialitys
                        ? specialitys.results?.map((speciality) => {
                            return (
                                <SpecialityRow speciality={speciality} key={speciality.id} />
                            )
                        })
                        : 'Cargando...'}
                </tbody>
            </table>

        </div>
    )
}