export function SpecialityElement({ details }) {
    return (
        <div>
            <div className="card" >
                <div className="py-3">
                    <img src="https://picsum.photos/100/100" className="card-img-top resize" style={{ maxWidth: '18rem', position: 'relative' }} />
                </div>
                <div className="card-body">
                    <h5 className="card-title">{details?.name}</h5>
                    <ul className="list-group">
                        <li className="list-group-item">{details?.description}</li>
                    </ul>
                </div>
            </div>
        </div>
    )
}