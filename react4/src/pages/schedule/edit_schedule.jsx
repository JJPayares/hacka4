import { Field, Form, Formik } from 'formik';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import axiosInstance from '../../services/axios_instance';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { updateElement } from '../../services/update_instance';


export function EditSchedule({ innerRef }) {
    const [data, setData] = useState(null)
    let [doctors, setDoctors] = useState(null)
    let [doctor, setDoctor] = useState(null)

    const nav = useNavigate();
    const params = useParams();



    const schedule_url = `schedules/${params.id}/`
    const current_doctor = `doctors/${data?.doctor_id}/`

    useEffect(() => {
        axiosInstance(schedule_url)
            .then((res) => {
                setData(res.data)
            })
            .catch((e) => e)

        axiosInstance('doctors/')
            .then((res) => {
                setDoctors(res.data)
            })
            .catch((e) => e)

        {
            data ? axiosInstance(current_doctor)
                .then((res) => {
                    setDoctor(res.data)
                })
                .catch((e) => e)
                : ''
        }
    }, [])

    let loggedUser = localStorage.getItem('user')

    const submit = (scheduleValues, actions) => {
        actions.setSubmitting(true);
        scheduleValues.modified_by = loggedUser

        //console.log(scheduleValues)

        delete scheduleValues.id, delete scheduleValues.modified_at
        delete scheduleValues.registered_at, delete scheduleValues.treatment_end
        delete scheduleValues.treatment_start
        updateElement(scheduleValues, schedule_url)
        actions.resetForm();

        actions.setSubmitting(false);
        nav('/schedules/')
    };

    return (
        <>
            {data ? <Formik
                initialValues={data}
                onSubmit={submit}
                innerRef={innerRef}
            >
                {({ }) => {
                    return (
                        <Form>
                            <div
                                className="form-group p-3"
                                style={{
                                    outline: '1px solid blue',
                                    borderRadius: '10px',
                                }}
                            >
                                <label htmlFor="treated_at">Fecha de tratamiento</label>
                                <Field
                                    className="form-control"
                                    id="treated_at"
                                    type="date"
                                    name="treated_at"
                                    placeholder="Ingrese la fecha de tratamiento."
                                    required
                                />

                                <label htmlFor="doctor_id" required>Doctor</label>
                                <Field name="doctor_id" as="select" def>
                                    <option value="default" disabled>Seleccione un doctor</option>
                                    {doctor ? <option value={data.doctor_id}>{`${doctor.name}`}</option> : ''}
                                    {doctors ? doctors?.results.map((doctor) => {
                                        return (
                                            <option key={doctor.id} value={doctor.id}>{`${doctor.name} ${doctor.last_name}`}</option>)
                                    }) : "Cargando..."}
                                </Field>
                                <br />
                                <label htmlFor="active">Estado</label>
                                <Field name="active" as="select">
                                    <option value={true}>Activo</option>
                                    <option value={false}>Inactivo</option>
                                </Field>
                                <button type="submit">Agregar</button>
                            </div>
                        </Form>
                    );
                }}
            </Formik> : 'Cargando...'}
        </>
    );
}
