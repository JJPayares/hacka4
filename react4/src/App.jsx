import './App.css'
import { Navbar } from './components/navbar'
import { Routes, Route, Outlet, useNavigate, useLocation, Navigate } from 'react-router-dom';
import { Doctor } from './pages/doctor/doctor_index';
import { LoginPage } from './pages/login/login_page';
import { AddDoctor } from './pages/doctor/add_doctor';
import { DoctorDetails } from './pages/doctor/doctor_details';
import { EditDoctor } from './pages/doctor/edit_doctor';
import { ConstructionPage } from './components/construcition_page';
import { Speciality } from './pages/speciality/speciality_index';
import { SpecialityDetails } from './pages/speciality/speciality_details';
import { Schedule } from './pages/schedule/schedule_index';
import { ScheduleDetails } from './pages/schedule/schedule_details';
import { AddSchedule } from './pages/schedule/add_schedule';
import { EditSchedule } from './pages/schedule/edit_schedule';
import { Patients } from './pages/patient/patient_index';
import { PatientDetails } from './pages/patient/patient_detail';
import { AddPatient } from './pages/patient/add_patient';
import { EditPatient } from './pages/patient/edit_patient';
import { Appointment } from './pages/appointment/appointment_index';
import { AppointmentDetails } from './pages/appointment/appointment_detail';
import { AddAppointment } from './pages/appointment/add_appointment';
import { EditAppointment } from './pages/appointment/edit_appointment';


function App() {
  return (
    <div className="App">
      <Navbar />
      <Routes>
        <Route path="/login/" element={<LoginPage />} />


        <Route path="doctors/" element={<PrivateRoute />}>
          <Route path="" element={<Doctor />} />
          <Route path="new/" element={<AddDoctor />} />
          <Route path=":id/" element={<DoctorDetails />} />
          <Route path="edit/:id/" element={<EditDoctor />} />
        </Route>

        <Route path="specialitys/" element={<PrivateRoute />}>
          <Route path="" element={<Speciality />} />
          <Route path=":id/" element={<SpecialityDetails />} />
        </Route>

        <Route path="schedules/" element={<PrivateRoute />}>
          <Route path="" element={<Schedule />} />
          <Route path="new/" element={<AddSchedule />} />
          <Route path=":id/" element={<ScheduleDetails />} />
          <Route path="edit/:id/" element={<EditSchedule />} />
        </Route>

        <Route path="patients/" element={<PrivateRoute />}>
          <Route path="" element={<Patients />} />
          <Route path="new/" element={<AddPatient />} />
          <Route path=":id/" element={<PatientDetails />} />
          <Route path="edit/:id/" element={<EditPatient />} />
        </Route>

        <Route path="appointments/" element={<PrivateRoute />}>
          <Route path="" element={<Appointment />} />
          <Route path="new/" element={<AddAppointment />} />
          <Route path=":id/" element={<AppointmentDetails />} />
          <Route path="edit/:id/" element={<EditAppointment />} />
        </Route>


        <Route path="*"
          element={
            <ConstructionPage />
          }
        />
      </Routes>
    </div>
  )
}

export default App;


const PrivateRoute = () => {
  const nav = useNavigate();
  const location = useLocation();
  const canAccess = !!localStorage.getItem("access_token");
  return (
    <div>

      <button
        className="btn btn-primary"
        onClick={() => {
          localStorage.removeItem("access_token");
          nav("/login/");
        }}
      >
        Logout
      </button>

      {canAccess ? (
        <Outlet />
      ) : (
        <Navigate to="/login/" replace state={{ url: location.pathname }} />
      )}
    </div>
  );
};
