from django.db import models


# Create your models here.
class Doctor(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    dni = models.CharField(max_length=50)
    address = models.CharField(max_length=150)
    email = models.EmailField()
    phone = models.CharField(max_length=50)
    gender = models.CharField(max_length=50)
    tuition_num = models.CharField(max_length=50)
    born_date = models.DateField()
    registered_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True, blank=True)
    registered_by = models.CharField(max_length=50, blank=True)
    modified_by = models.CharField(max_length=50, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.name} {self.last_name}'


class Specialty(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=150)
    registered_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True, blank=True)
    registered_by = models.CharField(max_length=50, blank=True)
    modified_by = models.CharField(max_length=50, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.name}'


class Doctor_Specialty(models.Model):
    doctor_id = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    specialty_id = models.ForeignKey(Specialty, on_delete=models.CASCADE)
    registered_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True, blank=True)
    registered_by = models.CharField(max_length=50, blank=True)
    modified_by = models.CharField(max_length=50, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.doctor_id} {self.specialty_id}'


class Schedule(models.Model):
    doctor_id = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    treated_at = models.DateField()
    treatment_start = models.DateTimeField(auto_now_add=True)
    treatment_end = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)
    registered_at = models.DateTimeField(auto_now_add=True)
    registered_by = models.CharField(max_length=50, blank=True)
    modified_at = models.DateTimeField(auto_now=True, blank=True)
    modified_by = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return f'{self.doctor_id} {self.treated_at}'

class Patient(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    dni = models.CharField(max_length=50)
    address = models.CharField(max_length=150)
    phone = models.CharField(max_length=50)
    gender = models.CharField(max_length=50)
    born_date = models.DateField()
    registered_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True, blank=True)
    registered_by = models.CharField(max_length=50, blank=True)
    modified_by = models.CharField(max_length=50, blank=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.name} {self.last_name}'


class Appointment(models.Model):
    doctor_id = models.ForeignKey(Doctor, on_delete=models.CASCADE)
    patient_id = models.ForeignKey(Patient, on_delete=models.CASCADE)
    treated_at = models.DateField()
    treatment_start = models.DateTimeField(auto_now_add=True)
    treatment_end = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=True)
    description = models.CharField(max_length=150)
    active = models.BooleanField(default=True)
    registered_at = models.DateTimeField(auto_now_add=True)
    registered_by = models.CharField(max_length=50, blank=True)
    modified_at = models.DateTimeField(auto_now=True, blank=True)
    modified_by = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return f'{self.doctor_id} - {self.patient_id} - {self.treated_at}'