import axiosInstance from '../../services/axios_instance';
import { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Field, Form, Formik } from 'formik';
import { updateElement } from '../../services/update_instance';


export function EditPatient({ innerRef }) {
    const [data, setData] = useState(null)
    const params = useParams()

    let loggedUser = localStorage.getItem('user')
    const nav = useNavigate()


    const url = `patients/${params.id}/`
    useEffect(() => {
        axiosInstance(url)
            .then((response) => setData(response.data))
            .catch((e) => console.log(e.response))
    }, [])

    const submit = (patientValues, actions) => {
        actions.setSubmitting(true);
        //console.log(patientValues)
        delete patientValues.id, delete patientValues.registered_at, delete patientValues.registered_by,
            patientValues.modified_by = loggedUser

        updateElement(patientValues, url)
        actions.resetForm();
        actions.setSubmitting(false);
        nav(-1)
    };

    return (
        <div>
            {data ? <Formik
                initialValues={data}
                onSubmit={submit}
                innerRef={innerRef}
            >
                {({ }) => {
                    return (
                        <Form>
                            <div
                                className="form-group p-3"
                                style={{
                                    outline: '1px solid blue',
                                    borderRadius: '10px',
                                }}
                            >
                                <label htmlFor="name">Nombre</label>
                                <Field
                                    className="form-control"
                                    id="name"
                                    type="text"
                                    name="name"
                                    placeholder="Ingrese el nombre del paciente."
                                    required
                                />

                                <label htmlFor="last_name">Apellido</label>
                                <Field
                                    className="form-control"
                                    id="last_name"
                                    type="text"
                                    name="last_name"
                                    placeholder="Ingrese el apellido del paciente."
                                    required
                                />

                                <label htmlFor="dni">Documento de identidad</label>
                                <Field
                                    className="form-control"
                                    id="dni"
                                    type="text"
                                    name="dni"
                                    placeholder="Ingrese el DNI del paciente."
                                    required
                                />

                                <label htmlFor="address">Direccion</label>
                                <Field
                                    className="form-control"
                                    id="address"
                                    type="text"
                                    name="address"
                                    placeholder="Ingrese la direcion del paciente."
                                    required
                                />

                                <label htmlFor="phone">Telefono</label>
                                <Field
                                    className="form-control"
                                    id="phone"
                                    type="text"
                                    name="phone"
                                    placeholder="Ingrese el numero de telefono del paciente."
                                    required
                                />

                                <label htmlFor="gender">Genero</label>
                                <Field name="gender" as="select" def>
                                    <option value="Femenino">Femenino</option>
                                    <option value="Masculino">Masculino</option>
                                    <option value="Otros">Otros</option>
                                </Field>

                                <label htmlFor="born_date">Fecha de nacimiento</label>
                                <Field
                                    className="form-control"
                                    id="born_date"
                                    type="date"
                                    name="born_date"
                                    placeholder="Ingrese la fecha de naciemiento del paciente."
                                    required
                                />

                                <br />
                                <label htmlFor="active">Estado</label>
                                <Field name="active" as="select">
                                    <option value={true}>Activo</option>
                                    <option value={false}>Inactivo</option>
                                </Field>

                                <button type="submit">Agregar</button>
                            </div>
                        </Form>
                    );
                }}
            </Formik> : <h1>Loading...</h1>}
        </div>
    );
}
