export function ConstructionPage() {
    const url_img = "https://sayingimages.com/wp-content/uploads/this-site-is-still-under-construction-memes.jpg" //"https://i.pinimg.com/736x/bd/8b/65/bd8b6556abfdd01b8208bc8ce4c926da.jpg"
    return (
        <div>
            <h1>Estamos trabajando en esta pagina...</h1>
            <h3>Regresa otro dia</h3>
            <div>
                <img src={url_img} className="card-img-top resize" style={{ maxWidth: '18rem', position: 'relative' }} />

            </div>
        </div>
    )
}