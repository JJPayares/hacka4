# Generated by Django 3.2.13 on 2022-06-04 16:19

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Doctor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('dni', models.CharField(max_length=50)),
                ('address', models.CharField(max_length=150)),
                ('phone', models.CharField(max_length=50)),
                ('gender', models.CharField(max_length=50)),
                ('tuition_num', models.CharField(max_length=50)),
                ('born_date', models.DateField()),
                ('registered_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('active', models.BooleanField(default=True)),
                ('modified_by', models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, related_name='modified_by_doctor', to=settings.AUTH_USER_MODEL)),
                ('registered_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='registered_by_doctor', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Specialty',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=150)),
                ('registered_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('active', models.BooleanField(default=True)),
                ('modified_by', models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, related_name='modified_by_speciality', to=settings.AUTH_USER_MODEL)),
                ('registered_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='registered_by_speciality', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Doctor_Specialty',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('registered_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('active', models.BooleanField(default=True)),
                ('doctor_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.doctor')),
                ('modified_by', models.ForeignKey(default='', on_delete=django.db.models.deletion.CASCADE, related_name='modified_by_doctor_speciality', to=settings.AUTH_USER_MODEL)),
                ('registered_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='registered_by_doctor_speciality', to=settings.AUTH_USER_MODEL)),
                ('specialty_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.specialty')),
            ],
        ),
    ]
