
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axiosInstance from '../../services/axios_instance';
import { ScheduleElement } from './schedule_element';

export function ScheduleDetails() {
    const [details, setDetails] = useState(null)
    const params = useParams()
    const url = `schedules/${params.id}/`

    useEffect(() => {
        axiosInstance({
            url: url, method: 'get'
        }).then((response) => setDetails(response.data))
            .catch((e) => {
                return e.response
            })
    }, [])
    

    return (
        <div>
            <ScheduleElement details={details} />
        </div>
    )
}