
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axiosInstance from '../../services/axios_instance';
import { SpecialityElement } from './speciality_element';

export function SpecialityDetails() {
    const [details, setDetails] = useState(null)
    const params = useParams()
    const url = `specialitys/${params.id}/`

    useEffect(() => {
        axiosInstance({
            url: url, method: 'get'
        }).then((response) => setDetails(response.data))
            .catch((e) => {
                return e.response
            })
    }, [])


    return (
        <div>
            <SpecialityElement details={details} />
        </div>
    )
}