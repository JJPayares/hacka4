import { Field, Form, Formik } from 'formik';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import axiosInstance from '../../services/axios_instance';
import { useEffect, useState } from 'react';
import { createElement } from '../../services/create_instance';



export function NewScheduleForm({ innerRef }) {
    const nav = useNavigate()
    let [doctors, setDoctors] = useState(null)

    const new_schedule_url = 'schedules/'

    useEffect(() => {
        axiosInstance('doctors/')
            .then((res) => setDoctors(res.data))
            .catch((e) => e)
    }, [])

    let loggedUser = localStorage.getItem('user')
    const datos_iniciales = {
        treated_at: "",
        doctor_id: "default",
        registered_by: loggedUser,
    }

    const submit = (scheduleValues, actions) => {
        actions.setSubmitting(true);
        //console.log(scheduleValues)
        actions.resetForm();
        createElement(scheduleValues, new_schedule_url)
        actions.setSubmitting(false);
        nav('/schedules/')
    };

    return (
        <Formik
            initialValues={datos_iniciales}
            onSubmit={submit}
            innerRef={innerRef}
        >
            {({ }) => {
                return (
                    <Form>
                        <div
                            className="form-group p-3"
                            style={{
                                outline: '1px solid blue',
                                borderRadius: '10px',
                            }}
                        >
                            <label htmlFor="treated_at">Fecha de tratamiento</label>
                            <Field
                                className="form-control"
                                id="treated_at"
                                type="date"
                                name="treated_at"
                                placeholder="Ingrese la fecha de tratamiento."
                                required
                            />

                            <label htmlFor="doctor_id" required>Doctor</label>
                            <Field name="doctor_id" as="select" def>
                                <option value="default" disabled>Seleccione un doctor</option>
                                {doctors ? doctors?.results.map((doctor) => {
                                    return (
                                        <option key={doctor.id} value={doctor.id}>{`${doctor.name} ${doctor.last_name}`}</option>)
                                }) : "Cargando..."}
                            </Field>

                            <button type="submit">Agregar</button>
                        </div>
                    </Form>
                );
            }}
        </Formik>
    );
}
