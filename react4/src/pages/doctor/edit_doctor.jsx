import axiosInstance from '../../services/axios_instance';
import { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Field, Form, Formik } from 'formik';
import { updateElement } from '../../services/update_instance';


export function EditDoctor({ innerRef }) {
    const [data, setData] = useState(null)
    const params = useParams()

    let loggedUser = localStorage.getItem('user')
    const nav = useNavigate()


    const url = `doctors/${params.id}/`
    useEffect(() => {
        axiosInstance(url)
            .then((response) => setData(response.data))
            .catch((e) => console.log(e.response))
    }, [])

    const submit = (doctorValues, actions) => {
        actions.setSubmitting(true);

        delete doctorValues.id, delete doctorValues.modified_at, delete doctorValues.registered_at, delete doctorValues.registered_by

        doctorValues.modified_by = loggedUser
        console.log((doctorValues))
        updateElement(doctorValues, url)
        actions.resetForm();
        actions.setSubmitting(false);
        nav(-1)
    };

    return (
        <div>
            {data ? <Formik
                initialValues={data}
                onSubmit={submit}
                innerRef={innerRef}
            >
                {({ }) => {
                    return (
                        <Form>
                            <div
                                className="form-group p-3"
                                style={{
                                    outline: '1px solid blue',
                                    borderRadius: '10px',
                                }}
                            >
                                <label htmlFor="name">Nombre</label>
                                <Field
                                    className="form-control"
                                    id="name"
                                    type="text"
                                    name="name"
                                    placeholder="Ingrese el nombre del medico."
                                    required
                                />

                                <label htmlFor="last_name">Apellido</label>
                                <Field
                                    className="form-control"
                                    id="last_name"
                                    type="text"
                                    name="last_name"
                                    placeholder="Ingrese el apellido del medico."
                                    required
                                />

                                <label htmlFor="dni">Documento de identidad</label>
                                <Field
                                    className="form-control"
                                    id="dni"
                                    type="text"
                                    name="dni"
                                    placeholder="Ingrese el DNI del medico."
                                    required
                                />

                                <label htmlFor="address">Direccion</label>
                                <Field
                                    className="form-control"
                                    id="address"
                                    type="text"
                                    name="address"
                                    placeholder="Ingrese la direcion del medico."
                                    required
                                />

                                <label htmlFor="email">Correo electronico</label>
                                <Field
                                    className="form-control"
                                    id="email"
                                    type="email"
                                    name="email"
                                    placeholder="Ingrese el correo electronico del medico."
                                    required
                                />

                                <label htmlFor="phone">Telefono</label>
                                <Field
                                    className="form-control"
                                    id="phone"
                                    type="text"
                                    name="phone"
                                    placeholder="Ingrese el numero de elefono del medico."
                                    required
                                />

                                <label htmlFor="gender">Genero</label>
                                <Field name="gender" as="select" required>
                                    <option value="Femenino">Femenino</option>
                                    <option value="Masculino">Masculino</option>
                                    <option value="Otros">Otros</option>

                                </Field>

                                <label htmlFor="tuition_num">Num Colegiatura</label>
                                <Field
                                    className="form-control"
                                    id="tuition_num"
                                    type="text"
                                    name="tuition_num"
                                    placeholder="Ingrese el num. de colegiatura del medico."
                                    required
                                />

                                <label htmlFor="born_date">Fecha de nacimiento</label>
                                <Field
                                    className="form-control"
                                    id="born_date"
                                    type="date"
                                    name="born_date"
                                    placeholder="Ingrese la fecha de naciemiento del medico."
                                    required
                                />

                                <label htmlFor="active">Estado</label>
                                <Field name="active" as="select">
                                    <option value={true}>Activo</option>
                                    <option value={false}>Inactivo</option>
                                </Field>

                                <button type="submit">Editar</button>
                            </div>
                        </Form>
                    );
                }}
            </Formik> : <h1>Loading...</h1>}
        </div>
    );
}
