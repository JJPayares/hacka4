import { Field, Form, Formik } from 'formik';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import axiosInstance from '../../services/axios_instance';
import { useEffect, useState } from 'react';
import { createElement } from '../../services/create_instance';



export function NewAppointmentForm({ innerRef }) {
    const nav = useNavigate()
    let [doctors, setDoctors] = useState(null)
    let [patients, setPatients] = useState(null)

    const new_appointment_url = 'appointments/'

    useEffect(() => {
        axiosInstance('doctors/')
            .then((res) => setDoctors(res.data))
            .catch((e) => e)

        axiosInstance('patients/')
            .then((res) => setPatients(res.data))
            .catch((e) => e)
    }, [])

    let loggedUser = localStorage.getItem('user')
    const datos_iniciales = {
        treated_at: "",
        description: "",
        doctor_id: "default",
        patient_id: "",
        registered_by: loggedUser,
    }

    const submit = (appointmentsValues, actions) => {
        actions.setSubmitting(true);
        console.log(appointmentsValues)
        //actions.resetForm();
        createElement(appointmentsValues, new_appointment_url)
        actions.setSubmitting(false);
        //nav('/appointments/')
    };

    return (
        <Formik
            initialValues={datos_iniciales}
            onSubmit={submit}
            innerRef={innerRef}
        >
            {({ }) => {
                return (
                    <Form>
                        <div
                            className="form-group p-3"
                            style={{
                                outline: '1px solid blue',
                                borderRadius: '10px',
                            }}
                        >
                            <label htmlFor="treated_at">Fecha de tratamiento</label>
                            <Field
                                className="form-control"
                                id="treated_at"
                                type="date"
                                name="treated_at"
                                placeholder="Ingrese la fecha de tratamiento."
                                required
                            />
                            <br />

                            <label htmlFor="doctor_id" required>Doctor</label>
                            <Field name="doctor_id" as="select" def>
                                <option value="default" disabled>Seleccione un doctor</option>
                                {doctors ? doctors?.results.map((doctor) => {
                                    return (
                                        <option key={doctor.id} value={doctor.id}>{`${doctor.name} ${doctor.last_name}`}</option>)
                                }) : "Cargando..."}
                            </Field>
                            <br />

                            <label htmlFor="patient_id" required>Paciente</label>
                            <Field name="patient_id" as="select" def>
                                <option value="default" disabled>Seleccione un paciente</option>
                                {patients ? patients?.results.map((patient) => {
                                    return (
                                        <option key={patient.id} value={patient.id}>{`${patient.name} ${patient.last_name}`}</option>)
                                }) : "Cargando..."}
                            </Field>
                            <br />

                            <label htmlFor="description">Descripcion</label>
                            <Field
                                className="form-control"
                                id="description"
                                type="text"
                                name="description"
                                placeholder="Ingrese la descripcion de la cita."
                                required
                            />
                            <br />

                            <button type="submit">Agregar</button>
                        </div>
                    </Form>
                );
            }}
        </Formik>
    );
}
