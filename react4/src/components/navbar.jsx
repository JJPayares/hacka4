import { BasicDropdown } from "./basic_dropdown";

export function Navbar() {
    return (
        <div>
            <nav className="navbar navbar-expand-lg bg-light">
                <div className="container-fluid">
                    <a className="navbar-brand" href="/">Hackaton 4</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <a className="nav-link active" aria-current="page" href="/login/">Login</a>
                            </li>
                            <BasicDropdown modelo={"Doctor"} list_url="/doctors/" create_element_url="/doctors/new/" />
                            <BasicDropdown modelo={"Especialidad"} list_url="/specialitys/" />
                            <BasicDropdown modelo={"Horarios"} list_url="/schedules/" create_element_url="/schedules/new/" />
                            <BasicDropdown modelo={"Pacientes"} list_url="/patients/" create_element_url="/patients/new/" />
                            <BasicDropdown modelo={"Citas"} list_url="/appointments/" create_element_url="/appointments/new/" />

                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    )
}