import { useState, useEffect } from 'react';
import axiosInstance from '../../services/axios_instance';
import { ListAppointment } from './list_appointments';


export function Appointment() {
    let [appointments, setAppointment] = useState(null)
    const APPOINTMENT_URL = "appointments/"

    useEffect(() => {
        axiosInstance({ url: APPOINTMENT_URL })
            .then((response) => {
                let results = response.data
                setAppointment(results)
            })
    }, [])

    return (
        <>
            <h1>CITAS</h1>
            <div>
                {appointments
                    ? <ListAppointment appointments={appointments} />
                    : 'Cargando...'}
            </div>
        </>
    )
}