import { useEffect, useState } from 'react';
import { useParams } from "react-router-dom"
import axiosInstance from '../../services/axios_instance';
import { PatientCV } from './patient_cv';


export function PatientDetails() {
    const [details, setDetails] = useState(null)

    const params = useParams()
    const url = `patients/${params.id}/`

    useEffect(() => {
        axiosInstance({
            url: url, method: 'get'
        }).then((response) => setDetails(response.data))
            .catch((e) => {
                return e.response
            })
    }, [])

    return (
        <div>
            <PatientCV details={details} />
        </div>

    )
}