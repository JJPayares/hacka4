import { NewPatientForm } from './new_patient_form';

export function AddPatient() {
    return (
        <NewPatientForm />
    )
}