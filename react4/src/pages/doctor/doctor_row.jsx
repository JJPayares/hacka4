import { DetailButton } from "../../components/datail_button";


export function DoctorRow({ doctor }) {
    return (
        <tr>
            <th scope="row">{doctor.id}</th>
            <td>{doctor.name}</td>
            <td>{doctor.last_name}</td>
            <td>{doctor.phone}</td>
            <td> <DetailButton id={doctor.id} /> </td>
        </tr>
    )
}