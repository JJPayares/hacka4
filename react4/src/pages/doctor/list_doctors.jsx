import { DoctorRow } from './doctor_row';
export function ListDoctors({ doctors }) {
    return (
        <div>

            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Detail</th>
                    </tr>
                </thead>
                <tbody>
                    {doctors
                        ? doctors.results?.map((doctor) => {
                            return (
                                <DoctorRow doctor={doctor} key={doctor.id} />
                            )

                        })
                        : 'Cargando...'}
                </tbody>
            </table>

        </div>
    )
}