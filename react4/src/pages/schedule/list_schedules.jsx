
import { ScheduleRow } from './schedule_row';
export function ListSchedule({ schedules }) {

    return (
        <div>

            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Fecha tratamiento</th>
                        <th scope="col">Doctor</th>
                        <th scope="col">Detail</th>
                    </tr>
                </thead>
                <tbody>
                    {schedules
                        ? schedules.results?.map((schedule) => {
                            return (
                                <ScheduleRow schedule={schedule} key={schedule.id} />
                            )

                        })
                        : 'Cargando...'}
                </tbody>
            </table>

        </div>
    )
}