import { PatientRow } from "./patient_row"

export function ListPatients({ patients }) {
    return (
        <div>

            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Detail</th>
                    </tr>
                </thead>
                <tbody>
                    {patients
                        ? patients.results?.map((patient) => {
                            return (
                                <PatientRow patient={patient} />
                            )

                        })
                        : 'Cargando...'}
                </tbody>
            </table>

        </div>
    )
}