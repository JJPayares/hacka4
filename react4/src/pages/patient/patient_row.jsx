import { DetailButton } from "../../components/datail_button";


export function PatientRow({ patient }) {
    return (
        <tr>
            <th scope="row">{patient.id}</th>
            <td>{patient.name}</td>
            <td>{patient.last_name}</td>
            <td>{patient.phone}</td>
            <td> <DetailButton id={patient.id} /> </td>
        </tr>
    )
}